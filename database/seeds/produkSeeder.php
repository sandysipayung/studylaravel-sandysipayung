<?php

use Illuminate\Database\Seeder;

class produkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//       DB::table('produks')->insert([
//           'nama' => str_random(50),
//           'merek' => str_random(40),
//           'jumlah' => rand()*100
//        ]);
       
       $faker = Faker\Factory::create();
       $limit = 33;
       
       for($i=0; $i<$limit;$i++){
          DB::table('produks')->insert([
           'name' =>$faker->name,
           'merek' =>$faker->name,
           'jumlah' =>$faker->randomDigit,
        ]); 
       }
    }
}
