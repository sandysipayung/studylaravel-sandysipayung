<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('users')->insert([
           'name' => str_random(10),
           'email' => str_random(10).'@gmail.com',
           'password' => bcrypt('secret')
        ]);
       $this->call(produkSeeder::class);//insertnya satu satu aja, call untuk manggil produkSeeder
       $users = factory(App\User::class, 10)->create();//$users itu dari create users ada 10 data
       $this->call(ProfilSeeder::class);
//       $this->call(OrderSeeder::class);
//       $this->call(OrderListSeeder::class);
       $this->call(TautanSeeder::class);
    }
}
