<?php

use Illuminate\Database\Seeder;
use App\User as user;
use App\Profil as profil;

class ProfilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();
        for ($i = 0; $i < 10; $i++){
            $profil = new profil;
            $profil->name = $faker->name;
            $profil->telephone = $faker->PhoneNumber;
            $user = new user;
            $user->name = $faker->name;
            $user->email = $faker->unique()->safeEmail;
            $user->password = bcrypt('secret');
            $user->remember_token = str_random(10);
            $user->save();
            $user->profil()->save($profil);//manggil ke model User ada function profil
        }
    }
}