<?php

use Illuminate\Database\Seeder;

class TautanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
//        MEMBUAT FAKER MENJADI DATA INDONESIA . . $faker = Faker\Factory::create('us_US');
        $faker = Faker\Factory::create('id_ID');
        for ($i=0;$i<50;$i++){
            DB::table('artikels')->insert([
                'judul'=>$faker->name,
                'link'=>$faker->url,
                'isi'=>$faker->paragraph,
                'tanggal'=>$faker->dateTime
            ]);
            DB::table('bukus')->insert([
                'judul'=>$faker->name,
                'sinopsis'=>$faker->paragraph,
                'tahun_terbit'=>$faker->year(),
                'penerbit'=>$faker->name
            ]);
            DB::table('dokumens')->insert([
                'judul'=>$faker->name,
                'ringkasan'=>$faker->paragraph
            ]);
        }
    }
}
