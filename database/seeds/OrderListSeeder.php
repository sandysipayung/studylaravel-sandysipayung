<?php

use Illuminate\Database\Seeder;

use App\produk as Produk;
use App\Order;

class OrderListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();
        
        $limit = 50;
        
        $produk = Produk::get()->pluck('id_produk')->toArray();
        $order = Order::get()->pluck('id')->toArray();
        for ($i = 0; $i < $limit; $i++){
            DB::table('orderlists')->insert([
                'order_id' => $faker->randomElement($order),
                'produk_id' => $faker->randomElement($produk),
                'jumlah' => $faker->randomDigit
            ]);
        }
    }
}
