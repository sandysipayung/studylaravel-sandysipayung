<?php

use Illuminate\Database\Seeder;

use App\User as User;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        
        $limit = 33;
        
        $order = User::get()->pluck('id')->toArray();
            for ($i = 0; $i < $limit; $i++){
                DB::table('produks')->insert([
                'name' => $faker->name,
                'merek' => $faker->Company,
                'jumlah' => $faker->randomDigit,
            ]);
               DB::table('orders')->insert([
                   'user_id' => $faker->randomElement($order),
                   'orderDate' => $faker->DateTime,
                   'status' => $faker->numberBetween(1,2)
               ]);
        }
    }
}
