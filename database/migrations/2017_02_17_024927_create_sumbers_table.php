<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSumbersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('sumbers', function (Blueprint $table) {
            $table->integer('artikel_id')->unsigned()->nullable();
            $table->foreign('artikel_id')->references('id')->on('artikels');
            $table->nullableMorphs('sumber');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('sumbers', function(Blueprint $table) {
            $table->dropForeign(['artikel_id']);            //APABILA FOREGIN KEY HARUS DIBUAT INI BRO
        });
        Schema::dropIfExists('sumbers');
    }

}
