<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Orderlists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //composit ga ada primary nya
        Schema::create('orderlists', function (Blueprint $table){
            $table->integer('produk_id')->unsigned()->nullable();
            $table->foreign('produk_id')->references('id_produk')->on('produks');
            $table->integer('order_id')->unsigned()->nullable();
            $table->foreign('order_id')->references('id')->on('orders');
            $table->integer('jumlah')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('orderlists', function(Blueprint $table){
            $table->dropForeign(['produk_id']);
            $table->dropForeign(['order_id']);
        });
        Schema::dropIfExists('orderlists');
    }
}
