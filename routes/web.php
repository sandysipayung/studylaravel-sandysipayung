<?php
use Illuminate\Http\Request;
/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('welcome'); //manggil file welcome di resources-> views
});

//route tanpa parameter
Route::get('/lihatroute', function() {
    return 'lihat Aku';
});


Route::get('/lihatview', 'blogController@index');

Route::get('/admin', 'dashboardController@index');

Route::get('/content2', function () {
    return view('pages.content2'); //manggil file welcome di resources-> views
});

Route::post('/form', function() {
    dd($_POST);
});

Route::get("param/{nilai}", function($nilai) {
    return "parameternya adalah " . $nilai; //runningnya localhost:8000/param/terserah tulis apa
});

Route::get("parameter/{nilai1}/{nilai2}", function($nilai1, $nilai2) {
    return "parameternya adalah " . $nilai1 . "<br>Parameter Kedua " . $nilai2; //runningnya localhost:8000/parameter/aksi1/aksi2
});

Route::get("parameter2/{nilai1}/{nilai2?}", function($nilai1, $nilai2 = "Default") { //tanda tanya itu artinya opsional bisa dikasih bisa engga
    return "parameternya adalah " . $nilai1 . "<br>Parameter Kedua " . $nilai2; //runningnya localhost:8000/parameter2/aksi1/
});
//route regex
Route::get("regex/{nilai}", function($nilai) {
    return "parameternya adalah " . $nilai; //runningnya http://localhost:8000/regex/1234
})->where('nilai', '[0-9]+');

Route::get('/', function() {
    return view('mantap', ['nama' => 'siti', 'alamat' => 'jakarta']);
});

Route::get("grade/{nilai}", function($nilai) {
    return view('pages.grade', ['nilai' => $nilai]); //runningnya localhost:8000/param/terserah tulis apa
});

Route::get('/contoh', function () {
    return view('contoh'); //manggil file welcome di resources-> views
});

Route::get('/input', 'belajarCRUD@index');
Route::post('/prosesinput', 'belajarCRUD@lihatdata'); // /prosesinput ditulis lagi di file form : contoh bagian actionnya
//Route group
Route::group(['prefix' => 'product'], function() { //product yang pertama dipanggil
    Route::get('list', function() { //http://localhost:8000/product/list
        return "list menu";
    });
    Route::get('menu', function() { //http://localhost:8000/product/menu
        return "Halaman menu";
    });
    Route::get('/', function() {
        return "Halaman Root Produk"; //http://localhost:8000/product
    });
});

//isset di Laravel -> ditambah value di formnya bagian nama
Route::get('/', function() {
    $data = array();
    return view('pages.contoh2', $data);
});

//unless untuk yang tidak terpenuhi
Route::get("jika", function() {
    $data = array('total' => 100, 'isi' => 'bebas');
    return view('pages.jika', $data);
});

//for
Route::get('/for', function () {
    return view('pages.for');
});

//foreach menggunakan unassosiatif array karena ga dikasih nilai langsung
Route::get('ulang', function () {
    $data = array(
        'list' => array("Pertama", "Kedua", "Ketiga", "Keempat", "Kelima")
    );
    return view('pages.foreach', $data);
});

//cara lain, kondisinya di simpan di page lain
Route::get('ulang', function () {
    return view('pages.foreach2');
});

//forelse nilai array nya nulls
Route::get('forelse', function () {
    $data = array(
        'list' => array()
    );
    return view('pages.forelse', $data);
});

//menggunakan php native
Route::get('/custom', function () {
    return view('pages.custom');
});

//componen
Route::get('/component', function () {
    return view('pages.component');
});

//middleware
Route::get('aksesmid', [
    'middleware' => 'hakakses',
    'uses' => 'cobaAksesController@index'] //harus pake uses
);

//crud data
Route::group(['prefix' => 'produk'], function() {
    Route::get('/list1', 'belajarController@index'); //nampilin data ga sesuai abjad
    Route::get('/list2', 'belajarController@custom'); //nampilin data sesuai abjad
    Route::get('/list3/{id}', 'belajarController@data'); //nampilin data hanya id nya saja http://localhost:8000/produk/list3/3
    Route::get('/data', 'belajarController@datatable');
    Route::get('/semua', 'belajarController@semua');
    Route::get('/custom', 'belajarController@custom');
    Route::get('/custom/{id}', 'belajarController@data')
            ->where('id', '[0-9]+');
    Route::get('/custom/form', 'belajarController@add'); //udah http://localhost:8000/produk/custom/form/
    Route::post('/custom', 'belajarController@addproc'); //udah http://localhost:8000/produk/custom/form/36
    Route::get('/custom/form/{id}', 'belajarController@edit');
    Route::put('/custom/{id}', 'belajarController@editproc');
    Route::delete('/custom/{id}', 'belajarController@delete'); //ke http://localhost:8000/produk/semua
    //Route::post('/custom/','belajarController@validation');
});


//route resource tgl 14 feb
Route::resource('profil', 'ProfilController'); //untuk manggil method create http://localhost:8000/profil/create

Route::resource('order', 'OrderController');

Route::get('produk', 'OrderController@produk');

//route tgl 16
Route::get('accessor', 'ProfilController@index');
Route::get('mutator', 'ProfilController@store'); //belum nih bingung formnya blm ada

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::post('/masuk', 'JWTAuthController@login'); //memunculkan token karena dia METHID POST. diambil dari AUTH/LOGIN.BLADE.PHP
//MANYTHORUGHS
Route::get('/tautan', 'TautanController@simpan');
Route::get('/tautan/lihat', 'TautanController@lihat');
Route::get('/tautan/sumber', 'TautanController@sumber');
Route::get('/tautan/lihats', 'TautanController@lihats');


Route::get('scope', 'OrderController@produksi');
Route::get('chunk', 'OrderController@produksi2');

Route::get('data/req"', 'belajarController@store');

//RESPON BROOOOO FLASHHH
Route::get('flash', function() {
//    return redirect('hafal')->with('pesan',"Pesan Langsung");
    return redirect('hafal')->with('pesan', view('flash')->render());
});

Route::get('hafal', function() {
    return view('isifla');
});

//COOKIE
Route::get('set', function() {
    return response("Ngeset Cookie Lae")->cookie('data', 'kue enak');
});

Route::get('get', function(Request $request) {
    $kue = $request->cookie('data');
    echo $kue;
});

Route::get('repo', 'repoCtrl@lihat');
Route::get('repolain', 'repoCtrl@lihatlain');

