const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .combine([
       './public/dist/css/AdminLTE.min.css',
       './public/dist/css/skins/_all-skins.min.css',
       './public/plugins/morris/morris.css',
       './public/plugins/jvectormap/jquery-jvectormap-1.2.2.css',
       './public/plugins/datepicker/datepicker3.css',
       './public/plugins/daterangepicker/daterangepicker.css',
       './public/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',   
   ],'./public/css/head.css');
