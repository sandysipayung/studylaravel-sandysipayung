<html>
    <head>
        <title>PT SIFA</title>
        <link rel="stylesheet" type="text/css" href="css/app.css">
    </head>
    <body>
        <h3 class="col-md-4 col-md-offset-4">Isi Form Ini </h3>
        <div class="container-fluid">
            <div class="row">
                <form class="col-md-4 col-md-offset-4" method="post" action="prosesinput" enctype="multipart/form-data">
                    <div class="form-group">
                        <label >Nama</label>
                        <input type="text" name="nama" required="required" class="form-control col-md-7 col-xs-12" placeholder="Masukkan Nama Anda">
                    </div>
                    <div class="form-group">
                        <label>Alamat </label>
                        <textarea type="text" name="alamat" required="required" class="form-control col-md-7 col-xs-12" placeholder="Masukkan Alamat Anda"></textarea>
                    </div>
                    <div class="form-group">
                        <label>No Telepon </label>
                        <input type="number" id="tlpn" required="required" class="form-control col-md-7 col-xs-12" placeholder="Masukkan No Telepon Anda">
                    </div>
                    <div class="form-group">
                        <label>Email </label>
                        <input type="email" id="email" required="required" class="form-control col-md-7 col-xs-12" placeholder="Masukkan Email Anda">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">File input</label>
                        <input type="file" name="gambar">
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Daftar" class="btn btn-primary" >
                    </div>
                    
                </form>
            </div>
        </div>
    </body>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.css"></script>
</html>