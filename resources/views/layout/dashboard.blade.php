<!DOCTYPE html>
<html>
@include('include/head')

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

@include('include/header')
  <!-- Left side column. contains the logo and sidebar -->
@include('include/sidemenu')  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
      <!-- Main content -->
    @yield('content')
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @include('include/footer')  
</body>
</html>
