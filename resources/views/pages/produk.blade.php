@extends('layout.dashboard')

@section('content')
<!-- Add Produk -->
<div id="AddProduk" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <form method="post" action="/jabatan/add">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Input Produk</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Nama Produk</label>
                        <input type="text" name="name" required="required" class="form-control">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                    <div class="form-group">
                        <label>Merek</label>
                        <input type="text" name="merek" required="required" class="form-control">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                    <div class="form-group">
                        <label>Jumlah</label>
                        <input type="number" name="jumlah" required="required" class="form-control">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success" value="save">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Jabatan</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <button type="button" class="btn btn-round btn-primary" data-toggle="modal" data-target="#AddProduk">Tambah Data</button>
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Produk</th>
                                <th>Merek</th>
                                <th>Jumlah</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Baju </td>
                                <td>Sophie Marthin</td>
                                <td> 4</td>
                                <td>
                                    <a href="#" class="btn btn-info btn-xs update-form"  data-toggle="modal" data-target="#EditJabatan"><i class="fa fa-pencil"></i> Edit </a>
                                    <a href="#" type="submit" class="btn btn-danger btn-xs" onclick="return confirm('Yakin data ini akan dihapus?');" name="name" value="delete"><i class="fa fa-trash-o"></i> Delete </a>
                                </td>
                            </tr>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
@endsection()