@extends('layout.dashboard')
@section('content')
<section class="content-header">
      <h1>
        Form Input
        <small>Control panel</small>
      </h1>
</section>
<section class="content">
    <h1>
        @if ($total>50)
            Banyak sekali
        @elseif ($total>10)
            Lumayan Banyak
        @else
            Kurang banyak
        @endif
        
        @unless (@total<50)
            {{$isi}}
        @endunless
    </h1>
        
</section>
@endsection()