@extends('layout.dashboard')

@section('content')
<!-- Add Produk -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Jabatan</h3>
                </div>

                <div class=container>
                    <!-- Modal content-->
                    <form action="/produk/custom/{{ $id_produk or '' }}" method='post'>
                        {{method_field ('PUT')}}
                        {{ csrf_field () }}
                        <div class='form-group'>
                            Nama<input type='text' name='nama' class="form-control" value='{{ $nama or ''}}'><br/>
                        </div>
                        <div class='form-group'>
                            Merek<input type='text' name='merek' class="form-control" value='{{ $merek or ''}}'><br/>
                        </div>
                        <div class='form-group'>
                            Jumlah<input type='text' name='jumlah' class="form-control" value='{{ $jumlah or ''}}'><br/>
                        </div>
                        <div class='form-group'>
                            <button type='submit'>SIMPAN</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>        


<!-- Main content 
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Jabatan</h3>
                </div>
                 /.box-header 
                <div class="box-body">
                    <button type="button" class="btn btn-round btn-primary" data-toggle="modal" data-target="#AddProduk">Tambah Data</button>
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Produk</th>
                                <th>Merek</th>
                                <th>Jumlah</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Baju </td>
                                <td>Sophie Marthin</td>
                                <td> 4</td>
                                <td>
                                    <a href="#" class="btn btn-info btn-xs update-form"  data-toggle="modal" data-target="#EditJabatan"><i class="fa fa-pencil"></i> Edit </a>
                                    <a href="#" type="submit" class="btn btn-danger btn-xs" onclick="return confirm('Yakin data ini akan dihapus?');" name="name" value="delete"><i class="fa fa-trash-o"></i> Delete </a>
                                </td>
                            </tr>
                    </table>
                </div>
                 /.box-body 
            </div>
             /.box 
        </div>
         /.col 
    </div>
     /.row 
</section>-->
@endsection()