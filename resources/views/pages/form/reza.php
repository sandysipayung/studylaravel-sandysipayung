@extends('layout.dashboard')
@section('content')
<div class="col-md-12">
  <div class="box">
    <div class="box-header">
      <h3>List Data Yang Punya Utang</h3>
    </div>
    <div class="box-body">
              <table id="example1" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Nomor</th>
                  <th>Nama Supplier</th>
                  <th>Kota</th>
                  <th>Provinsi</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  @php
                    $i=1;
                  @endphp
                @foreach ($supplier as $datasupplier)
                  <tr>
                    <td>
                      {{$i}}
                    </td>
                    <td>
                      {{$datasupplier->nama_supplier}}
                    </td>
                    <td>
                      {{$datasupplier->kota}}
                    </td>
                    <td>
                      {{$datasupplier->provinsi}}
                    </td>
                    <td>
                      <a href="deletedatasupplier/{{$datasupplier->id}}" class="btn btn-default">Delete</a>
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#id{{$i}}">Edit</button>
                    </td>
                  </tr>

                  <div class="modal fade bs-example-modal-lg" id="id{{$i}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                    <div class="modal-dialog modal-lg" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title">Tambah Data</h4>
                        </div>
                        <div class="modal-body">
                          <form action="supplier" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                              <label for="exampleInputEmail1">Nama</label>
                              <input type="text" name="nama_supplier" class="form-control" placeholder="Nama Toko">
                            </div>
                            <div class="form-group">
                              <label for="exampleInputEmail1">Kota</label>
                              <input type="text" name="kota" class="form-control" placeholder="Kota Toko nya dimana">
                            </div>
                            <div class="form-group">
                              <label for="exampleInputEmail1">Provinsi</label>
                              <input type="text" name="provinsi" class="form-control" placeholder="Provinsi">
                            </div>
                        </div>
                        <div class="modal-footer">
                          <button type="submit" class="btn btn-default">Simpan</button>
                        </form>
                        </div>
                      </div>
                    </div>
                  </div>
                  @php
                    $i++;
                  @endphp
                @endforeach
              </tbody>
            </thead>
          </table>
      </div>
  </div>
  <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#tambahdata">Tambah Data</button>

  <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="tambahdata" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Tambah Data</h4>
        </div>
        <div class="modal-body">
          <form action="supplier" method="post">
            
            {{ csrf_field() }}
            <div class="form-group">
              <label for="exampleInputEmail1">Nama</label>
              <input type="text" name="nama_supplier" class="form-control" placeholder="Nama Toko">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Kota</label>
              <input type="text" name="kota" class="form-control" placeholder="Kota Toko nya dimana">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Provinsi</label>
              <input type="text" name="provinsi" class="form-control" placeholder="Provinsi">
            </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-default">Simpan</button>
        </form>
        </div>
      </div>
    </div>
  </div>
  </div>

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
@endsection
