@extends('layout.dashboard')
@section('content')
<!-- Add Produk -->
<section class="content">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3>List Data Yang Punya Utang</h3>
            </div>
            <div class="box-body">
                <div class=container>
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Produk</th>
                                <th>Merek</th>
                                <th>Jumlah</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Modal content-->
                            @each('pages.form.del',$data,'baris')
                        </tbody>
                        <a href='/produk/custom/form' class="btn btn-success">Tambah Data</a>
                    </table>
                    {{$data -> links()}}
                </div>
            </div>
        </div>
    </div>
</section>        

@endsection()