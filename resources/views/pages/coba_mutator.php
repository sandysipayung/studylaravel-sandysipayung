@extends('layout.dashboard')
@section('content')
<section class="content-header">
      <h1>
        Form Input
        <small>Control panel</small>
      </h1>
</section>
<section class="content">
    <h3 class="col-md-4 col-md-offset-4">Isi Form Ini </h3>
        <div class="container-fluid">
            <div class="row">
                <form action="form" method="post" class="col-md-4 col-md-offset-4">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label >Nama </label>
                        <input type="text" name="name" required="required" class="form-control col-md-7 col-xs-12" placeholder="Masukkan Nama Anda">
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Daftar" class="btn btn-primary" >
                    </div>
                    
                </form>
            </div>
        </div>
</section>
@endsection()