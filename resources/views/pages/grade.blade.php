<html>
    <head>
        <title>PT SIFA</title>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <form class="col-md-4 col-md-offset-4">
                    @if($nilai>=80&&$nilai<=100)
                    anda mendapatkan nilai A
                    @elseif($nilai>=70&&$nilai<=79)
                    anda mendapatkan nilai B
                    @elseif($nilai>=60&&$nilai<=69)
                    anda mendapatkan nilai C
                    @elseif($nilai>=50&&$nilai<=49)
                    anda mendapatkan nilai D
                    @else
                    anda mendapatkan nilai E
                    @endif
                    
                    <div class="form-group">
                        <label>Nilai {{$nilai}} </label>
                        
                    </div>
                    
                    
                </form>
            </div>
        </div>
    </body>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.css"></script>
</html>