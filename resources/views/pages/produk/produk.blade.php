@extends('layout.dashboard')

@section('content')
@if (count($errors)>0)
<div class="aler alert-danger">
    <ul>
    @foreach ($errors ->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
    </ul>
</div>

@endif
        <h3 class="col-md-4 col-md-offset-4">Isi Form Ini </h3>
        <div class="container-fluid">
            <div class="row">
                <form class="col-md-4 col-md-offset-4" action="/produk/custom/" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label >Nama </label>
                        <input type="text" name="nama" class="form-control col-md-7 col-xs-12">
                    </div>
                    <div class="form-group">
                        <label>Merek </label>
                        <input type="text" name="merek" class="form-control col-md-7 col-xs-12">
                    </div>
                    <div class="form-group">
                        <label>Jumlah </label>
                        <input type="number" name="jumlah" class="form-control col-md-7 col-xs-12" >
                    </div>
                    
                    <div class="form-group">
                        <button type="submit">Simpan</button>
                    </div>
                    
                </form>
            </div>
        </div>
  @endsection()