@extends('layout.dashboard')

@section('content')

        <h3 class="col-md-4 col-md-offset-4">Edit Form Produk </h3>
        <div class="container-fluid">
            <div class="row">
                <form class="col-md-4 col-md-offset-4" action="/produk/custom/{{ $id_produk or ''}}" method="post">
                    {{ method_field('PUT')}}
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label >Nama </label>
                        <input type="text" name="nama" value="{{ $name or '' }}" required="required" class="form-control col-md-7 col-xs-12">
                    </div>
                    <div class="form-group">
                        <label>Merek </label>
                        <input type="text" name="merek" value="{{ $merek or '' }}" required="required" class="form-control col-md-7 col-xs-12">
                    </div>
                    <div class="form-group">
                        <label>Jumlah </label>
                        <input type="number" name="jumlah" value="{{ $jumlah or '' }}" required="required" class="form-control col-md-7 col-xs-12" >
                    </div>
                    
                    <div class="form-group">
                        <input type="submit" value="Daftar" class="btn btn-primary" >
                    </div>
                    
                </form>
            </div>
        </div>
    @endsection()