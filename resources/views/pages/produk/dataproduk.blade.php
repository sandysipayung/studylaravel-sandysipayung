@extends('layout.dashboard')

@section('content')

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Produk</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <button type="button" class="btn btn-round btn-primary" data-toggle="modal" data-target="#AddProduk">Tambah Data</button>
                <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Nama Produk</th>
                  <th>Merek</th>
                  <th>Jumlah</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td>{{ $baris->name }}</td>
                  <td>{{ $baris->merek }}</td>
                  <td>{{ $baris->jumlah }}</td>
                </tr>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection()