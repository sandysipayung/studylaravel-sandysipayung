@extends('layout.dashboard')
@section('content')
<div class="container">
      @component('hal.alert')<!--folder hal file alert -->
        @slot('class')
            alert-danger
        @endslot
        @slot('title')
            Something is wrong
        @endslot
        My components with errors
      @endcomponent
      
      <!--success -->
      @component('hal.alert')
        @slot('class')
            alert-success
        @endslot
        @slot('title')
            success
        @endslot
        My components with success response
      @endcomponent
</div>
@endsection()