@extends('layout.dashboard')
@section('content')
<section class="content">
 @php
    //$data = null; //apabila datanya kosong dia nambilin kosong.blade
    $data = array("Pertama", "Kedua", "Ketiga", "Keempat", "Kelima")
 @endphp
    <div class="row">
        <div class="col-lg-3 col-xs-6">
           @each('pages.data', $data, 'baris', 'pages.kosong')<!-- 'baris' adalah variabel harus sama dengan yg di data.blade-->
        </div>
    </div>
        
</section>
@endsection()