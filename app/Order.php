<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    protected $dates = ['delete_at'];
    protected $fillable = ['user_id','orderDate','status'];


    public function user(){
        return $this->belongsTo('App\User');
    }
    
    public function produk(){
        return $this->belongsToMany('App\produk', 'orderlists', 'order_id', 'produk_id');//('Nama Model', 'nama tabel', 'pk table order', 'fk')
    }
}
