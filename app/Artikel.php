<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artikel extends Model {

    //
    public function tautan() {
        return $this->morphMany('App\Tautan', 'tautan');
    }

    public function buku() {
        return $this->morphedByMany('App\Buku', 'sumber');
    }

    public function dokumen() {
        return $this->morphedByMany('App\Dokumen', 'sumber');
    }

}
