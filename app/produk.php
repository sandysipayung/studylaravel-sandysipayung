<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class produk extends Model {

    use SoftDeletes;

    protected $primaryKey = 'id_produk'; //nambahin ini karena kita ngerubah id jadi id_produk
    protected $table = 'produks';
    protected $dates = ['delete_at'];
    protected $fillable = ['name', 'merek', 'jumlah'];

    public function order() {
        return $this->belongsToMany('App\Order', 'orderlists', 'produk_id', 'order_id')->withPivot('jumlah'); //('Nama Model', 'nama tabel', 'pk table produk', 'fk')
    }

    //DINAMIC SCOPE
    public function scopeLebih($query, $jumlah) {  //scopeLebih itu adalah format nya agar supaya diperhatikan L nya harus huruf besar
        return $query->where('jumlah', '>', $jumlah);
    }

    //STATIC SCOPE
    public function scopeLebih5($query) {
        return $query->where('jumlah', '>', 5);
    }

    public function scopeKurang($query, $jumlah) {  //scopeLebih itu adalah format nya agar supaya diperhatikan L nya harus huruf besar
        return $query->where('jumlah', '<', $jumlah);
    }
}
