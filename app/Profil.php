<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model {

    //
    public function user() {
        return $this->belongsTo('App\User');
    }

    public function order() {                    //DARI TABEL USER,   ORDER,    PROFILS
        return $this->hasManyThrough('App\Order', 'App\User', 'id', 'user_id', 'user_id');
    }

}
