<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    //Accessor untuk mengubah nilai dari kolom db ke tampilan, di akses di Profilcontroller
    public function getNamaemailAttribute(){//formatnya harus diawali dengan get dan diakhiri dengan attribue
        return $this->name.' '.$this->email;
    }
    
    public function getNameAttribute($value){
        return strtoupper($value); //strtoupper untuk membuat huruf menjadi kapital
    }
    
    //Mutator untuk manipulasi inputan. Liat function store di ProfilController
    public function setNameAttribute($value){
        $this->attributes['name'] = ucwords(strtolower($value));
    }

    public function profil(){
        return $this->hasOne('App\profil');//dipake di UserProfilSeeder
    }
    
    public function order(){
        return $this->hasMany('App\Order');
    }
}
