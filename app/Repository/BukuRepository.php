<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Repository;

use App\Repository\FilterrInterface;
use App\Buku as Buku;

class BukuRepository implements FilterInterface
{
    protected $buku;
    public function __construct(Buku $buku)
    {
        $this->buku= $buku;
    }
    
    public function cari()
    {
        return $this->buku->all();
    }
    
    public function cariLagi($id,$attr='judul')
    {
        return $this->buku->where($attr,$id);
    }
}
