<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    //
    public function tautan() {
        return $this->morphMany('App\Tautan','tautan');
    }
    
    public function sumber() {
        return $this->morphToMany('App\Artikel','sumber');
    }
}
