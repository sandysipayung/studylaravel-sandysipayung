<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tautan extends Model {

    //
    public function tautan() {
        return $this->morphTo();
    }

}
