<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Artikel as Artikel;
use App\Buku as Buku;
use App\Dokumen as Dokumen;
use App\Tautan as Tautan;

class TautanController extends Controller {

    //
    public function simpan() {
        $artikel = Artikel::find(2);
        $buku = Buku::find(2);
        $dokumen = Dokumen::find(2);
        $tautan = new Tautan;
        $tautan->nama = 'Tautan Artikel 1';
        $tautan->link = 'http://sandysipayung.wordpress.com';
        $artikel->tautan()->save($tautan);
        $tautan = new Tautan;
        $tautan->nama = 'Tautan Buku 1';
        $tautan->link = 'http://sandysipayung.wordpress.com';
        $buku->tautan()->save($tautan);
        $tautan = new Tautan;
        $tautan->nama = 'Tautan Dokumen 1';
        $tautan->link = 'http://sandysipayung.wordpress.com';
        $dokumen->tautan()->save($tautan);
    }

    public function lihat() {
        $artikel = Artikel::find(2);
        echo $artikel->judul . "<br/>";
        foreach ($artikel->tautan as $tautan) {
            echo "Nama Tautan = " . $tautan->nama . "<br/>";
            echo "Link Tautan = " . $tautan->link . "<br/>";
        }
    }

    public function sumber() {
        $buku = Buku::find(5);
        $dokumen = Dokumen::find(4);
        $buku->sumber()->attach([1, 2, 3]);
        $dokumen->sumber()->attach([3, 4, 5]);
    }

    public function lihats(Request $request) {
        $artikel = Artikel::find(3);
        foreach ($artikel->buku as $tautan) {
            echo "<br/>Sinopsis = " . $tautan->sinopsis;
        }
        foreach ($artikel->dokumen as $tautan) {
            echo "<br/>Ringkasan = " . $tautan->ringkasan;
        }
    }

}
