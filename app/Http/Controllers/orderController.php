<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User as User;
use App\Profil as Profil;
use App\Order as Order;
use App\produk as Produk;

class OrderController extends Controller {

    //nampilin nama usernya baru tgl ordernya
    public function index() {
        $data = User::all();
//        $data = Profil::all();
//        $data->load('order');  //(LAZY EAGER LOADING)
        $data = User::with('order')->get();  //EAGER LOADING, buat ngeload data supaya cepat
        foreach ($data as $user) {
            echo "Nama User = " . $user->name;
            //echo "<br/> Nama Profil = ". $user->profil->name; //dikomen karena ada profil yg null
            if (!empty($user->order)) {
                foreach ($user->order as $order) { //1 user bisa mesen banyak order
                    echo "<br/>Tanggal Order = " . $order->orderDate;
                }
            }
            echo "<br/>";
        }
        $data = User::find(656); //user_id nya 656 untuk saveMany, menyimpan bnyk order dari 1 user
//        $data = User::where('email', 'eryn07@example.net') //set otomatis ke dbnya http://localhost:8000/order
//                ->update(['name'=>"berubah"]);
        $data->order()->saveMany([
            new Order(['orderDate' => '2000-01-01', 'status' => '1']),
            new Order(['orderDate' => '2001-02-02', 'status' => '1']),
            new Order(['orderDate' => '2002-03-03', 'status' => '1'])
        ]);

        //running : refresh http://localhost:8000/order dan tabel order_listnya
        //attach-> nambah id_produk
//        $order = Order::find(5);//order_id
//        $order->produk()->attach([1,2,3,4,5]);//produk idnya nambah 1,2,3,4,5
//        
        //detach -> menghapus
//        $order = Order::find(5);//order_id
//        $order->produk()->detach([1,2]);//produk idnya ilang 1,2
        //syc -> melakukan 2 proses attach dan detach
        $order = Order::find(5); //order_id
        $order->produk()->sync([1 => ['jumlah' => 10], 2 => ['jumlah' => 5], 3]); //jadi 4,5 ga ada yg 1,2 tadi di syc lagi setelah di detach
        //updareExictingPovot
        $order = Order::find(5); //order_id
        $order->produk()->updateExistingPivot(2, ['jumlah' => 8]); //jadi ngerubah produk_id 2 jumlahnya 8 yang asalnya nilainya 5
    }

    //nampilin tgl ordernya dlu baru nama usernya
    public function create() {
        $data = Order::all();
        foreach ($data as $user) {
            echo "Tanggal Order = " . $user->orderDate;
            echo "<br/>Nama User = " . $user->user->name;
            echo "<br/>";
        }
    }

    //many to many
    public function produk() {
        $data = Produk::all();
//        $data=Produk::lebih5()->orderBy('jumlah','asc')->get();
        foreach ($data as $produk) {
            echo "Nama Produk = " . $produk->name;
            foreach ($produk->order as $order) {
                echo "<br/>Tanggal Order = " . $order->orderDate;
                echo "<br/>Jumlah Order = " . $order->pivot->jumlah;
            }
            echo "<br/>";
        }
    }

    //SCOPE BROOOOO
    public function produksi() {
//        $data = Produk::all();
//        $data=Produk::lebih5()->orderBy('jumlah','asc')->get();
//        $data=Produk::lebih(3)->orderBy('jumlah','asc')->get(); // diambil dari produk model yang disana SCOPE LEBIH
        $data = Produk::kurang(4)->orderBy('jumlah', 'asc')->get();
        foreach ($data as $produk) {
            echo "Nama Produk = " . $produk->name;
            echo "<br/>Jumlah Produk = " . $produk->jumlah;
            echo "<br/>";
        }
    }

    //CHUNK INI BROOOO
    public function produksi2() {
//        $data = Produk::all();
//        $data=Produk::lebih5()->orderBy('jumlah','asc')->get();
//        $data=Produk::lebih(3)->orderBy('jumlah','asc')->get(); // diambil dari produk model yang disana SCOPE LEBIH
        $data = Produk::kurang(4)->orderBy('jumlah', 'asc')->get();
        produk::chunk(5, function($data) {
            foreach ($data as $produk) {
                echo "Nama Produk = " . $produk->name;
                echo "<br/>Jumlah Produk = " . $produk->jumlah;
                echo "<br/>";
            }
        });
    }

}
