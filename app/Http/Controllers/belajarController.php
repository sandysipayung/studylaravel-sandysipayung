<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\produk as produk;

class belajarController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $produk = produk::all();
        foreach ($produk as $baris) {
            echo "<br/>Nama = " . $baris->name;
            echo "<br/>Merek = " . $baris->merek;
            echo "<br/>Jumlah = " . $baris->jumlah;
            echo "<br/>";
        }
    }

    public function datatable() {
        return view('pages.produk');
    }

    public function custom() {
        $produk = produk::orderBy('name', 'asc')->get();
        foreach ($produk as $baris) {
            echo "<br/>Nama = " . $baris->name;
            echo "<br/>Merek = " . $baris->merek;
            echo "<br/>Jumlah = " . $baris->jumlah;
            echo "<br/>";
        }
    }

    public function data($id) {
        $produk = produk::find($id);
        $produk = produk::findOrFail($id);
        echo "<br/>Nama = " . $produk->name;
        echo "<br/>Merek = " . $produk->merek;
        echo "<br/>Jumlah = " . $produk->jumlah;
        echo "<br/>";
    }

    //belajar tgl 13 Februari
    public function add() {
        return view('pages.produk.produk');
    }

    public function edit($id) {
        //untuk ngubah data
        $produk = produk::find($id);
        return view('pages.produk.produke', $produk);
    }

    public function addproc(Request $request) {
        $produk = new produk;
        $this->validate($request, [
            'nama' => 'required|max:255',
            'merek' => 'required',
            'jumlah' => 'required'
                ], [
            'nama.required' => 'Field Nama harus diisi',
            'merek.required' => 'Field Merek harus diisi',
            'jumlah.required' => 'Field Jumlah harus diisi'
        ]);

        $produk->name = $request->nama; //name dari field db di tabel produk, nama di formnya
        $produk->merek = $request->merek;
        $produk->jumlah = $request->jumlah;
        $produk->save();
        echo "Data telah disimpan";
        echo "<a href='/produk/custom/form' class='btn btn-info'>Form </a>";
    }

    //ke model produk tambahkan fillable untuk assesment
//    public function addproc2(Request $request){
//        $produk = new produk;
//        $data = array('nama'=>$request->nama,
//                'merek'=>$request->merek,
//                'jumlah'=>$request->jumlah);
//        
//        $produk->create($data);
//        echo "Data telah disimpan";
//        echo "<a href='/produk/custom/form' class='btn btn-info'>Form </a>";
//    }

    public function editproc(Request $request, $id) {
        $produk = produk::find($id);
        $produk->name = $request->nama;
        $produk->merek = $request->merek;
        $produk->jumlah = $request->jumlah;
        $produk->save();
        echo "Data telah diupdate";
        echo "<a href='/produk/custom/form' class='btn btn-info'>Form </a>";
    }

    public function delete($id) {
        $produk = produk::find($id);
        $produk->delete();
        echo "Data telah dihapus";
        echo "<a href='/produk/custom/form' class='btn btn-info'>Form </a>";
    }

    public function semua() {
        $produk = produk::all();
        return view('pages.produk.produkd', ['data' => $produk]);
        $produk->each(function($baris) {
            echo "<br/>Nama = " . $produk->name;
            echo "<br/>Merek = " . $produk->merek;
            echo "<br/>Jumlah = " . $produk->jumlah;
            echo "<br/>";

            $produk = produk::paginate(5);
            return view('pages.produk.produkd', ['produk' => $produk]);
        });
    }

    public function validation(Request $request) {
        $this->validate($request, [
            'nama' => 'required|max:255',
            'merek' => 'required',
            'jumlah' => 'required'
                ], [
            'nama.required' => 'Field Nama harus diisi',
            'merek.required' => 'Field Merek harus diisi',
            'jumlah.required' => 'Field Jumlah harus diisi'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //untuk post nnti dikirim ke store
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
//    public function store(Request $request) {
//        //proses untuk insert/add
//        echo "Root = " . $request->root();
//        echo "<br/>URL = " . $request->url();
//        echo "<br/>Full = " . $request->fullurl();
//        echo "<br/>Full URL Query= ";
//        var_dump($request->fullurlwithquery(array('tes' => 'ini')));
//        echo "<br/>Path = " . $request->path();
//        echo "<br/>Decode Path = " . $request->decodePath();
//        echo "<br/>Segment = " . $request->segment(1);
//        echo "<br/>Segments = ";
//        var_dump($request->segments());
//        echo "<br/>AJAX = ";
//        var_dump($request->ajax());
//        echo "<br/>IP = " . $request->ip();
//        echo "<br/>Secure = ";
//        var_dump($request->secure());
//    }
    public function store(Request $request) {
        $request->session()->put('nama', 'sesi');  //SESSION SESSION
        echo "Sesi = ";
        var_dump($request->session()->all());
        echo "<br/>Get = ", $request->get('coba');
        echo "<br/>Root = " . $request->root();
        echo "<br/>URL = " . $request->url();
        echo "<br/>Full = " . $request->fullurl();
        echo "<br/>URL = " . $request->url();
        echo "<br/>Full URL Query = ";
        var_dump($request->fullUrlWithQuery(array('tes' => 'ini')));
        echo "<br/>Path = " . $request->path();
        echo "<br/>Decode Path = " . $request->decodedPath();
        echo "<br/>Segment = " . $request->segment(1);
        echo "<br/>Segments = ";
        var_dump($request->segments());
        echo "<br/>AJAX = ";
        var_dump($request->ajax());
        echo "<br/>IP = " . $request->ip();
        echo "<br/>Secure = ";
        var_dump($request->secure());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //untuk nampilin data
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //ke database
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //untuk delete
    }

}
