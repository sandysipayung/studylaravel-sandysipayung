<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Auth;

class JWTAuthController extends Controller {

//
    public function login(Request $request) {
        $credentials = $request->only('email', 'password');
        try {
//verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return redirect('login')->with('status', 'Login GAGAL, Cek Username dan Password');
            }
        } catch (Exception $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
//if no errors are encountered we can return a JWT
//        return response()->json(compact('token'));
        return response()->json(compact('token')).'<br/><br/>'.Auth::user();  //MEMANGGIL SI TOKEN DAN HASIL DATABASE :)
//        echo Auth::user();
    }

}
//SEMUA CODING DIATAS UNTUK PENGECEKAN TOKEN DAN MEMUNCULKAN  ERROR KALAU ADA