<?php

namespace App\Http\Middleware;

use Closure;

class HakAksesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        echo "Membuat akses middelware";
        return $next($request);
        //simpen ifnya disini
    }
    
    public function terminate($request, $respone)
    {
        //store the session data..
        echo "<br/> Akhir dari suatu middleware controller";
    }
}
