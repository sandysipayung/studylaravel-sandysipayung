<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dokumen extends Model {

    //
    public function tautan() {
        return $this->morphMany('App\Tautan', 'tautan');
    }

    public function sumber() {
        return $this->morphToMany('App\Artikel', 'sumber');
    }

}
